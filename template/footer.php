<!-- Footer -->
<footer class="text-white pt-3 pb-2" style="background-color: #29534e;">
    <div class="d-flex flex-row justify-content-center">
     
    <a href="https://www.gob.mx/bachilleres" target="_blank">
        <img src="images/footer/logo-CBweb.svg" class="logoBchfooter"></a>
    <img src="images/footer/somoslobos.png"  class="logosomos img-fluid mr-4">
    <a href="https://www.facebook.com/ColegioBachilleres/" target="_blank">
        <img src="images/footer/facebook-icon.svg" class="icons mr-2 img-fluid mr-4"></a>
    <a href="https://twitter.com/CdeBachilleres" target="_blank">
        <img src="images/footer/twitter-icon.svg" class="icons img-fluid mr-4"></a>
    </div>
</footer>
    
    

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- control menu movile -->
    <script type="text/javascript">
        var mrp = false;
        $("#boton-menu-responsivo").on('click', function () {
            if (mrp == false) {
                $("#gaceta-menu").addClass("mrp-show");
                mrp = true;
            } else {
                $("#gaceta-menu").removeClass("mrp-show");
                mrp = false;
            }
        });
    </script>
</body>

</html>