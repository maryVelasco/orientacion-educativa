<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Orientación educativa</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="template/css/styles.css?v=951264" rel="stylesheet">

    <!-- font Awesome CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- google font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700&amp;subset=latin"
        media="all">
    <link rel="shortcut icon" href="images/header/favicon.ico">
</head>

<body>
    <div class="debug"></div>
    
<header>
    <div class="container-head">
        <!-- logo educacon y colbach-->
        <div class="tiraInts">
             <!-- sea grega la clase inst para poder controlar adecuadamente el div -->
             <div class="inst">
                <img class="tiraimg img-fluid" src="images/header/tira-inst.svg">
            </div>
            <!-- se agrega div con imagen del menu para moviles -->
            <div id="boton-menu-responsivo">
                <img src="images/header/menu_responsivo.svg" />
            </div>
           
        </div>

        <!-- Fila logo gaceta y editorial-->
        <div class="row-Gaceta">
            <div class="logo-legal">
                <div class="LGaceta">
                    <img src="images/header/gacetaCB-logo.svg">
                </div>
                <div class="div-legal">
                    <div class="texLegal">Órgano Informativo del Colegio de Bachilleres</div>
                </div>
            </div>
        </div>

        <!-- Fila texto organo informativo-->
        <div class="filaOrgano">
            <div class="textOrgano">Abril de 2020 / año I / Número 4</div>
        </div>

        <?php include("menu.php"); ?>
        
    </div>
</header>