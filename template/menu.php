<!-- Menu gaceta-->
<?php

$smenu = array(
	array( 'link' => '', 'title' => 'Propósito', 			'img' => '' ),
	array( 'link' => '', 'title' => 'Tutor', 				'img' => '',
		'opcions' => array(
			array( 'link' => '', 'img' => '', 'title' => 'Bienvenida' ),
			array( 'link' => '', 'img' => '', 'title' => 'Programa de tutorías' ),
			array( 'link' => '', 'img' => '', 'title' => 'Acciones para la prevención de la violencia' ),
			array( 'link' => '', 'img' => '', 'title' => 'Desarrollo de las habilidades socioemocionales' ),
			array( 'link' => '', 'img' => '', 'title' => 'Exposición interactiva de movilidadsocial' ),
			array( 'link' => '', 'img' => '', 'title' => 'ESRU' ),
		),
	),
	array( 'link' => '', 'title' => 'Padres de familia', 	'img' => '',
		'opcions' => array(
			array( 'link' => '', 'img' => '', 'title' => 'Bienvenida' ),
			array( 'link' => '', 'img' => '', 'title' => 'Situaciones de riesgo' ),
			array( 'link' => '', 'img' => '', 'title' => 'Sitios de interés' ),
			array( 'link' => '', 'img' => '', 'title' => 'Temas de interés' ),
		),
	),
	array( 'link' => '', 'title' => 'Orientador', 			'img' => '',
		'opcions' => array(
			array( 'link' => '', 'img' => '', 'title' => 'Área psicopedagógica' ),
			array( 'link' => '', 'img' => '', 'title' => 'Área vocacional' ),
			array( 'link' => '', 'img' => '', 'title' => 'Visitas a secundarias' ),
			array( 'link' => '', 'img' => '', 'title' => 'Área Psicosocial y fomento a la salud' ),
			array( 'link' => '', 'img' => '', 'title' => 'Jornada de prevención de adicciones' ),
			array( 'link' => '', 'img' => '', 'title' => 'Promoción de la salud con pasantes de enfermería' ),
			array( 'link' => '', 'img' => '', 'title' => 'Prevenimss' ),
		),
	),
	array( 'link' => '', 'title' => 'Alumnos', 				'img' => '',
		'opcions' => array(
			array( 'link' => '', 'img' => '', 'title' => 'Bienvenida' ),
			array( 'link' => '', 'img' => '', 'title' => 'Asignaturas' ),
			array( 'link' => '', 'img' => '', 'title' => 'Programas y mecanismos remediales' ),
			array( 'link' => '', 'img' => '', 'title' => 'Plan de carrera' ),
			array( 'link' => '', 'img' => '', 'title' => 'Decide to vacación' ),
			array( 'link' => '', 'img' => '', 'title' => 'Convocatorias' ),
			array( 'link' => '', 'img' => '', 'title' => 'Prevención de la violencia' ),
			array( 'link' => '', 'img' => '', 'title' => 'Temas de interés' ),
		),
	),
	array( 'link' => '', 'title' => 'Inicio', 				'img' => 'home.svg' ),
);

if( $smenu ){
	?><div id="gaceta-menu"><ul><?php
	foreach ($smenu as $et => $r) {
		?><li class=""><a href="<?php echo $r['link'] ?>" title="<?php echo $r['title'] ?>"><?php echo $r['title'] ?></a><?php

		if( isset($r['opcions']) ){
			?><ul><?php
			foreach ($r['opcions'] as $etr => $rr) {
				?><li class=""><a href="<?php echo $rr['link'] ?>" title="<?php echo $rr['title'] ?>"><?php echo $rr['title'] ?></a></li><?php
			}
			?></ul><?php
		}

		?></li><?php
	}
	?></ul></div><?php
}

?>